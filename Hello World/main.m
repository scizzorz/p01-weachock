//
//  main.m
//  Hello World
//
//  Created by John Weachock on 2/1/16.
//  Copyright © 2016 John Weachock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

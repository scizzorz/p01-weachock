//
//  ViewController.m
//  Hello World
//
//  Created by John Weachock on 2/1/16.
//  Copyright © 2016 John Weachock. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize hello_label;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clicked:(id)sender
{
    [hello_label setText:@"John Weachock"];
}

- (IBAction)clicked1:(id)sender
{
    [hello_label setText:@"1"];
}

- (IBAction)clicked2:(id)sender
{
    [hello_label setText:@"2"];
}

- (IBAction)clicked3:(id)sender
{
    [hello_label setText:@"3"];
}

@end

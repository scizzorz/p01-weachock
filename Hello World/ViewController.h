//
//  ViewController.h
//  Hello World
//
//  Created by John Weachock on 2/1/16.
//  Copyright © 2016 John Weachock. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *hello_label;

-(IBAction)clicked:(id)sender;
-(IBAction)clicked1:(id)sender;
-(IBAction)clicked2:(id)sender;
-(IBAction)clicked3:(id)sender;


@end

